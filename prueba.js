(function ($) {
   $(document).ready(function() {
      $('#ej2').click(function() {
         if ($('#val2').val() == '') {
            alert('must input some value for this exercise');
            return false;
         }
         else result('ej2', $('#val2').val());
      });

      $('#ej1').click(function() {
         result('ej1', false);
      });

      var result = function(ej, param) {
         switch(ej) {
            case 'ej1': var url = 'https://reqres.in/api/users?page=1'; break;
            case 'ej2': var url = 'https://reqres.in/api/users?page='+param; break;
         }

         $.ajax({
            type: 'get',
            url: url,
            dataType:'json',
            data:{},
            success: function (response) {
               switch(ej) {
                  case 'ej1': ejercicio1(response.data); break;
                  case 'ej2': ejercicio2(response.data); break;
               }
            }
         });
      };

      var ejercicio1 = function(data) {
         var cant = 0;

         $(data).each(function() {
           var elem = $(this)[0];
           var descr= elem.first_name+elem.last_name;

           console.log(descr);

           if (descr.length > 12) {
              cant++;
           }
         });

         var avg = cant / data.length * 100;
         var res = $('<li style="padding:10px; margin:10px 0; border:1px solid black">'+
                     '<label>'+avg+'% have first name + last name with more than 12 characters</label>'+
                     '<br><i>I assumed that what was requested in point 1 is to work on page 1, so I clarify that I detect that no user has a combination of name + last name with more than 12 characters</i>'+
                     '</li>');

         $('#result').append(res);
      };

      var ejercicio2 = function(data) {
         $('#result').empty();

         $(data).each(function() {
           var elem = $(this)[0];

           var user = $('<li style="padding:10px; margin:10px 0; border:1px solid black">');
           var name = $('<div>');
           var mail = $('<a>');
           var avat = $('<img>');

           avat.attr('src', elem.avatar);
           mail.attr('href', 'mailto:'+elem.email);
           mail.text(elem.email);
           name.text(elem.first_name+' '+elem.last_name);

           user.append(avat);
           user.append(name);
           user.append(mail);

           $('#result').append(user);
         });
      };
   });
})(jQuery);
